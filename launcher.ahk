; Stream Deck Launcher for Walden's Automation 
;
; HOW TO USE:
; - In command line arguments enter the full path of the script you want to open
; - That script will be detected by the path name, and closed or opened accordingly

; get a list of script paths from command line arguments
script_paths := [] 
for n, param in A_Args
{
    script_paths.Push(param)
}

if (script_paths.MaxIndex() == 0) 
{
    MsgBox Please provide script paths to open in arguments
}

; ahk scripts are hidden windows when in the task bar
; so must do this to detect / close  scripts
DetectHiddenWindows, On 

; Closing/Opening all scripts
for index, path in script_paths
{
    if (WinExist(path "ahk_class AutoHotkey"))
    {
        WinClose, %path% ahk_class AutoHotkey
    } 
    else 
    {
        Run %path%
    }
}
